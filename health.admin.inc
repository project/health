<?php
/**
 * @file
 * Contains admin forms.
 */

/**
 * Admin settings form.
 */
function health_admin_settings_form($form, &$form_state) {
  $form['email'] = array(
    '#type' => 'fieldset',
    '#title' => t('E-mail settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['email']['health_email_threshold'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Get e-mails if health status is:'),
    '#options' => array(
      HEALTH_OKAY => t('Okay'),
      HEALTH_UNKNOWN => t('Unknown'),
      HEALTH_WARNING => t('Warning'),
      HEALTH_ERROR => t('Error'),
    ),
    '#default_value' => variable_get('health_email_threshold', array()),
  );
  $form['email']['health_email_frequency'] = array(
    '#type' => 'select',
    '#title' => t('E-mail sending frequency'),
    '#options' => array(
      'never' => t('Never'),
      3600 * 24 * 7 => t('Once a week'),
      3600 * 24 * 1 => t('Once per day'),
      3600 * 12 => t('Twice a day'),
      3600 => t('Hourly'),
      'cron' => t('Every cron run'),
    ),
    '#description' => t('The speed e-mails are sent depends on how often cron runs.'),
    '#default_value' => variable_get('health_email_frequency', 'never'),
  );
  $form['email']['health_email_subject'] = array(
    '#type' => 'textfield',
    '#title' => t('Health e-mail subject'),
    '#default_value' => variable_get('health_email_subject'),
  );
  $form['email']['health_email_users'] = array(
    '#type' => 'textarea',
    '#title' => t('Send health status e-mails to:'),
    '#description' => t('Enter each e-mail on a new line.'),
    '#default_value' => variable_get('health_email_users'),
  );
  $form['api'] = array(
    '#type' => 'fieldset',
    '#title' => t('API settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['api']['health_api_require_https'] = array(
    '#type' => 'checkbox',
    '#title' => t('Require HTTPS (recommended)'),
    '#description' => t('This will ensure that all API requests are secure.'),
    '#default_value' => variable_get('health_api_require_https'),
  );
  $form['api']['health_api_access_key'] = array(
    '#type' => 'textfield',
    '#title' => t('API Access key'),
    '#description' => t('Private key that is required to be sent via POST to access the API. !help',
      array('!help' => l(t("What is this?"), "admin/help/health", array('fragment' => 'api-key')))),
    '#default_value' => variable_get('health_api_access_key'),
  );
  return system_settings_form($form);
}

/**
 * Health dashboard with data from previous days.
 */
function health_admin_dashboard_form($form, &$form_state) {
  $data = health_get_data();
  $form['title'] = array(
    '#markup' => '<h1>' . t('!site status', array('!site' => variable_get('site_name'))) . '</h1>',
  );
  if (!$data) {
    // No monitors created yet, let them know.
    $form['no_monitors'] = array(
      '#markup' => t('You have not created any monitors yet. !help',
        array('!help' => l(t('View help'), "admin/help/health"))),
    );
  }
  $monitors = health_get_monitors();
  foreach ($data as $group => $results) {
    // Create a fieldset for new groups.
    $form['groups'][$group] = array(
      '#type' => 'fieldset',
      '#title' => $group,
      '#collapsible' => TRUE,
    );
    $form['groups'][$group]['results'] = array(
      '#theme' => 'table',
      '#rows' => health_admin_dashboard_rows($monitors[$group], $results),
      '#header' => health_admin_dashboard_headers(),
    );
  }
  return $form;
}

/**
 * Gets the admin dashboard rows for a particular set of results.
 */
function health_admin_dashboard_rows($monitors, $results) {
  $rows = array();
  foreach ($monitors as $key => $m) {
    $status = $results[$key]['status'];
    $status_icon_path = url(drupal_get_path('module', 'health') . '/css/status-icon-' . $status, array('absolute' => TRUE));
    $rows[] = array(
      'class' => array('health-status-' . $status),
      'data' => array(
        array(
          'data' => '<img alt="' . $status . '" title="' . $status . '" width=30 src="' . $status_icon_path . '.png"/>',
          'width' => 30,
        ),
        $results[$key]['message'],
        $m['name'],
        $m['description'],
      ),
    );
  }
  return $rows;
}

/**
 * Gets the headers for the admin dashboard table.
 */
function health_admin_dashboard_headers() {
  return array(
    t('Status'),
    t('Status Message'),
    t('Monitor'),
    t('Description'),
  );
}
