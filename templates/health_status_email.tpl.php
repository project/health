<?php
/**
 * @file
 * Template for the health status e-mail.
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
  <title>Health Status for <?php print $site_name ?></title>
</head>
<body>
<h1>Health status for <?php print $site_name ?>: <?php print format_date(REQUEST_TIME, 'short') ?></h1>
<hr/>
<?php foreach ($tables as $group => $t): ?>
  <h2><?php print $group ?></h2>
  <?php print $t ?>
<?php endforeach; ?>
<br/>
<br/>
<hr/>
<p style="font-size:9pt;color:#888;">
  To stop receiving this e-mail, <?php print l(t('modify Health Status settings'), 'admin/config/system/health', array('absolute' => TRUE)) ?>.<br/>
  Trouble viewing this e-mail? <?php print l(t('View the Health Status dashboard'), 'admin/reports/health/dashboard', array('absolute' => TRUE)) ?>.
</p>
</body>
</html>
