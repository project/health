<?php
/**
 * @file
 * Health monitor help template.
 *
 * @link admin/help/health
 */
?>
<h1>Health monitor</h1>
<hr/>
<h2 id="about">About</h2>
<p>
  The Health monitor system allows you to easily hook into, and create monitors
  that report site functionality.
</p>
<h2 id="monitors">Monitors</h2>
<p>
  Monitors are the meat of the Health module. A monitor is essentially a function
  that returns a Health status. To create a new monitor, you need to implement
  <code>hook_health_monitors()</code>.
  <br/>
  Example:
  <br/>
<pre>function hook_health_monitors() {
  $monitors['check_user_email'] = array(
    'name' => t('Check user e-mail'),
    'description' => t('Ensures user e-mail addresses are valid.'),
    'group' => t('Users'),
  );
  return $monitors;
}</pre>

Once you register a monitor, you need to create the monitor function.<br/>
Your monitor should implements <code>hook_health_monitor_MONITOR()</code>.
<br/>
Example:
<br/>
<pre>function hook_health_monitor_MONITOR($args) {
  if (check_something()) {
    return array(
      'status' => HEALTH_OKAY,
      'message' => t("This is what we want, don't worry."),
    );
  }
  else {
    return array(
      'status' => HEALTH_ERROR,
      'message' => t('Something is not true!'),
    );
  }
}</pre>
</p>
<h2 id="email">Emails</h2>
<p>
  Health monitor allows you to easily schedule e-mails to be sent showing the
  status of different monitors. You can set e-mails to only be sent if there
  is an "error" with one of your monitors.
</p>

<h2 id="api">API</h2>
<p>
  The Health monitor module comes with a built-in API. This allows 3rd party
  systems to access your site's health status. The API is "private", meaning
  a 3rd party system <b>must</b> POST a secret key in order to access the data.
</p>
<h3 id="api-key">API Key</h3>
<p>
  The API secret key can be set in the admin settings page. This key must be
  POSTed to <b>/health/api/%method</b>.
</p>
<h3 id="api-methods">API Methods</h3>
<p>
  The API comes with some useful return methods, like JSON and XML. To return
  you site health status in different formats, simply change the last URL
  argument.<br/>
  To return JSON: <code>/health/api/json</code>
  <br/>
  To return XML: <code>/health/api/xml</code>
  <br/><br/>
  You can easily create new return formats by implementing
  <code>hook_health_api_callback()</code>.
  <br/>
  Example to return a serialized PHP array:<br/>
<pre>function hook_health_api_callback_php($data) {
  return serialize($data);
}</pre>
</p>
