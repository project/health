<?php
/**
 * @file
 * Contains functions related to getting/showing data in the API.
 */

/**
 * Implements hook_health_api_callback_METHOD().
 *
 * Returns data in proper JSON format.
 */
function health_health_api_callback_json($data) {
  drupal_add_http_header("Content-type", "application/json");
  return drupal_json_encode($data);
}

/**
 * Implements hook_health_api_callback_METHOD().
 */
function health_health_api_callback_var_export($data) {
  return var_export($data, TRUE);
}

/**
 * Implements hook_health_api_callback_METHOD().
 */
function health_health_api_callback_print_r($data) {
  return print_r($data);
}

/**
 * Implements hook_health_api_callback_METHOD().
 *
 * Returns data as XML.
 */
function health_health_api_callback_xml($data) {
  $xml = new SimpleXMLElement("<health></health>");
  $groups = $xml->addChild("groups");
  foreach ($data as $gr => $results) {
    $group = $groups->addChild("group");
    $group->addAttribute("name", $gr);
    $monitors = $group->addChild("monitors");
    foreach ($results as $key => $r) {
      $m = $monitors->addChild("monitor");
      $m->addAttribute("name", $key);
      $m->addChild("status", $r['status']);
      $m->addChild("message", $r['message']);
    }
  }
  return $xml->asXML();
}

/**
 * Health API callback.
 *
 * This callback will return differing results based on the $method.
 * A valid $method could be json, drush, print_r, var_export, print.
 * Other modules can hook into and add their own methods.
 *
 * @param string $method
 *   A return method.
 */
function health_api_callback($method) {
  $health_data = health_api_get_data();
  $results = module_invoke_all("health_api_callback_" . $method, $health_data);
  foreach ($results as $r) {
    print $r;
  }
}

/**
 * Gets data that can be used to display in the API.
 */
function health_api_get_data() {
  return health_get_data();
}