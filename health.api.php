<?php
/**
 * @file
 * This file contains examples and documentation of the various hooks
 * you can use to add Health monitors to the Health Dashboard.
 */

/**
 * Allows you too create custom API callback handlers.
 *
 * API requests are made to: health/api/METHOD
 * The hook should return some custom format of the $data that is passed to it.
 *
 * @param array $data
 *   All health result data.
 *
 * @return string
 *   Returns a string to be rendered in a custom format.
 */
function hook_health_api_callback_METHOD($data) {
  // Will only return "error" or "okay" based on custom method.
  foreach ($data as $groups) {
    foreach ($groups as $check) {
      if ($check['status'] == HEALTH_ERROR) {
        // This check has an error, return "error".
        return "error";
      }
    }
  }
  return "okay";
}

/**
 * Allows you to add custom monitors to the Health Dashboard.
 *
 * Format is:
 *   $monitors[NAME] = array(
 *     'name' => 'Name of monitor',
 *     'description' => 'Description of monitor.',
 *     'group' => 'Monitors are shown in groups on the dashboard.',
 *     'args' => array(), // Array of args that is sent to monitor hooks.
 *   );
 *
 * @return array
 *   An array of monitors.
 */
function hook_health_monitors() {
  // Make sure user mail addresses are valid.
  $monitors['check_user_email'] = array(
    'name' => t('Check user e-mail'),
    'description' => t('Ensures user e-mail addresses are valid.'),
    'group' => t('Users'),
  );
  // Checks to see if a value exists in some table.
  $monitors['value_in_table'] = array(
    'name' => t('Value in table check'),
    'description' => t('Makes sure the value is in the table.'),
    'group' => t('Misc'),
    'args' => array('value' => 'something'),
  );
  return $monitors;
}

/**
 * This hook is called when the health system runs the monitors.
 *
 * If you had a monitor called "value_in_table", your hook would be:
 * hook_health_monitor_value_in_table($args).
 *
 * @param array $args
 *   Args that have been passed from hook_health_monitor().
 *
 * @return array
 *   This hook needs to return an array containing a status, and an (optional)
 *   message. The status can be HEALTH_OKAY, HEALTH_ERROR, HEALTH_WARNING or
 *   HEALTH_UNKNOWN. The message is just a string that will show up on the
 *   dashboard.
 */
function hook_health_monitor_MONITOR($args) {
  if (check_something()) {
    return health_monitor_status(HEALTH_OKAY, t("This is what we want, don't worry."));
  }
  else {
    return health_monitor_status(HEALTH_ERROR, t('Something is not true!'));
  }
}

/**
 * Allows you to alter the health data array before it gets returned.
 *
 * @param array $data
 *   Health data array.
 *
 * @see health_get_data()
 */
function hook_health_data_alter(&$data) {

}

/**
 * Allows you to alter the monitors array before it is used.
 *
 * @param array $monitors
 *   Health monitors.
 *
 * @see health_get_monitors()
 */
function hook_health_monitors_alter(&$monitors) {

}
